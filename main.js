/**
 * Created by 911 on 29/6/2016.
 */

var express = require("express");

var app = express();

// Proccess request
app.get("/time", function(req, res) {
    var currTime = new Date();
    //set status code
    res.status(202);
    //set content type - MIME
    res.type("text/html");
    //custom headers
    res.set("X-My-Header", "hello");
    //send the result
    // res.send("The current time is: " + currTime);
    res.send("<html><body><h1>The current time is: " + currTime + "</h1></body></html>");
});

app.get("/time", function(req, res) {
    var currTime = new Date();
    //set status code
    res.status(202);
    //set content type - MIME
    res.type("text/html");
    //custom headers
    res.set("X-My-Header", "hello");
    //send the result
    // res.send("The current time is: " + currTime);
    res.send("<html><body><h1>The current time is: " + currTime + "</h1></body></html>");
});

var expressStatic = express.static(__dirname + "/public");

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/bower_components"));


app.use(function(req, res){
    res.redirect("/error.html");
});

//Set our port
app.set ( "port",
    process.argv[2] || process.env.APP_PORT || 3000);

app.listen(app.get("port"), function(){
    console.info("Application started on port %d", app.get("port"));
});



